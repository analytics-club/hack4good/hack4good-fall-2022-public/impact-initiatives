import os
from pathlib import Path
import pandas as pd
import zipfile

from pandas import DataFrame

from constants import (
    raw_surveys_path,
    raw_audits_folder,
    results_folder,
    surveys_path,
    audits_path,
    enumerators_path,
    nodes_path,
    deindexed_nodes_path,
    merged_raw_audits_path,
    c_uuid,
    c_enum_id,
    src_data_folder,
    questionnaire_path,
    anomaly_results_path,
    results_shap_folder,
    results_enumerator_folder,
    results_features_folder,
    src_surveys_path,
    src_surveys_sheet,
    src_questionaire_path,
    src_questionaire_sheet,
    src_audits_folder,
    src_audits_zip,
)


def _unzip_if_needed(path: Path):
    """Unzip file at `path` if this hasn't already been done."""
    dst_path = path.with_suffix("")
    if not dst_path.exists():
        with zipfile.ZipFile(path, "r") as zip_ref:
            zip_ref.extractall(dst_path)


def _data_to_parquet():
    """Read data from excel and csv formats and write into ./data/ in more efficient parquet format."""
    raw_audits_folder.mkdir(parents=True, exist_ok=True)

    pd.read_excel(src_surveys_path, sheet_name=src_surveys_sheet).sort_values(
        by=[c_enum_id, "start"]
    ).drop_duplicates(c_uuid, keep="first").set_index(c_uuid).to_parquet(
        raw_surveys_path
    )

    pd.read_excel(src_questionaire_path, sheet_name=src_questionaire_sheet)[
        ["type", "name", "relevant", "choice_filter", "calculation", "constraint"]
    ].to_parquet(questionnaire_path)

    _unzip_if_needed(src_audits_zip)
    uuids = os.listdir(src_audits_folder)
    for uuid in uuids:
        dst_audit_path = raw_audits_folder / f"{uuid}.parquet"
        pd.read_csv(src_audits_folder / str(uuid) / "audit.csv").to_parquet(
            dst_audit_path
        )


def cached_collect_raw_data():
    """Load survey responses and audit files as .parquet files from ./data/"""
    if not raw_audits_folder.is_dir():
        _data_to_parquet()
    surveys = pd.read_parquet(raw_surveys_path)
    audit_uuids = set(filename[:-8] for filename in os.listdir(raw_audits_folder))
    survey_uuids = set(surveys.index.tolist())
    uuids = audit_uuids.intersection(survey_uuids)
    return (
        surveys,
        {
            uuid: pd.read_parquet(raw_audits_folder / f"{uuid}.parquet")
            for uuid in uuids
        },
    )


def _collect_audits_df():
    """Join audits with their survey's uuid and enumerator id and concatenate into one df."""
    surveys, audits_dict = cached_collect_raw_data()
    for (uuid, audit_df) in audits_dict.items():
        audit_df[c_uuid] = uuid
        audit_df["enumerator"] = surveys.loc[uuid, c_enum_id]
    audits_df = (
        pd.concat(audits_dict.values())
        .sort_values([c_uuid, "start"])
        .reset_index(drop=True)
    )
    audits_df.to_parquet(merged_raw_audits_path)
    return audits_df


def _cached_collect_audits_df():
    """Loads `merged_raw_audits.parquet` if it exists,
    else generates it calling `_collect_audits_df()`."""
    try:
        return pd.read_parquet(merged_raw_audits_path)
    except FileNotFoundError:
        return _collect_audits_df()


def cached_collect_merged_raw_data():
    """:return `raw_surveys.parquet`, `merged_raw_audits.parquet`, and `questionnaire.parquet`
    loading these from file if they exist else doing whatever it takes
    to generate them."""
    if (
        not raw_surveys_path.is_file()
        or not questionnaire_path.is_file()
        or not raw_audits_folder.is_dir()
    ):
        _data_to_parquet()
    raw_surveys = pd.read_parquet(raw_surveys_path)
    merged_raw_audits = _cached_collect_audits_df()
    questionnaire = pd.read_parquet(questionnaire_path)
    return raw_surveys, merged_raw_audits, questionnaire


def persist_processed(
    enumerators: DataFrame,
    surveys: DataFrame,
    audits: DataFrame,
    nodes: DataFrame,
    deindexed_nodes: DataFrame,
):
    """Write to parquet files in `data_folder`."""
    enumerators.to_parquet(enumerators_path)
    surveys.to_parquet(surveys_path)
    audits.to_parquet(audits_path)
    nodes.to_parquet(nodes_path)
    deindexed_nodes.to_parquet(deindexed_nodes_path)


def load_processed():
    """:return enumerators, surveys, audits, nodes, deindexed_nodes,
    read from parquet files in `data_folder`."""
    return (
        pd.read_parquet(enumerators_path),
        pd.read_parquet(surveys_path),
        pd.read_parquet(audits_path),
        pd.read_parquet(nodes_path),
        pd.read_parquet(deindexed_nodes_path),
    )


def prepare_results_folder():
    results_folder.mkdir(parents=True, exist_ok=True)
    results_shap_folder.mkdir(parents=True, exist_ok=True)
    results_enumerator_folder.mkdir(parents=True, exist_ok=True)
    results_features_folder.mkdir(parents=True, exist_ok=True)


def write_final_results_to_excel(
    IF_outliers: DataFrame,
    outliers_shap: DataFrame,
    enumerators: DataFrame,
    typical_summary: DataFrame,
    anomalies_summary: DataFrame,
):
    final_outlier_df = pd.concat(
        [
            IF_outliers,
            outliers_shap[
                [
                    "first_important_feature",
                    "second_important_feature",
                    "third_important_feature",
                ]
            ],
        ],
        axis=1,
    ).drop("survey_type", axis=1)
    with pd.ExcelWriter(anomaly_results_path) as writer:

        final_outlier_df.to_excel(writer, sheet_name="Anomalies")

        outliers_shap.to_excel(writer, sheet_name="SHAP Values")

        enumerators[
            ["region", "num_surveys", "num_suspicious", "percent_sus"]
        ].to_excel(writer, sheet_name="Enumerators")
        typical_summary.to_excel(writer, sheet_name="Typical Surveys Features")
        anomalies_summary.to_excel(writer, sheet_name="Anomaly Surveys Features")
