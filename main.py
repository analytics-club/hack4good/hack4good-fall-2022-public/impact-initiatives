from constants import (
    c_unique_question_duration_correlation,
    c_active_fraction_before_long_break,
    c_largest_relative_pace_increase,
    c_median_log_relative_pace,
    c_active_seconds_spent,
    c_enum_sec_on_surveys_that_day,
)

from feature_engineering import make_featurized_dataframes
from storage_handler import load_processed
from anomaly_detection.isolation_forest_anomaly_detection import perform_and_save_anomaly_detection

enumerators, surveys, audits, nodes, deindexed_nodes = make_featurized_dataframes()
# enumerators, surveys, audits, nodes, deindexed_nodes = load_processed()

desired_features_anomaly_detection = [
    c_unique_question_duration_correlation,
    c_active_fraction_before_long_break,
    c_largest_relative_pace_increase,
    c_median_log_relative_pace,
    "log_longer_shorter_median_pace_ratio",
    c_active_seconds_spent,
    c_enum_sec_on_surveys_that_day,
    "duration",
    "time_per_question",
    "variance",
    "num_constraint_notes",
    "constraint_backtracks",
    "resume_count",
    "constraint_errors",
    "median_active_seconds_per_survey",
]


perform_and_save_anomaly_detection(
    enumerators, surveys, desired_features_anomaly_detection, plot=True
)

print("Success")
