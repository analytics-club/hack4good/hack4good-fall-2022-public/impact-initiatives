import matplotlib.pyplot as plt
import seaborn as sns
from constants import (
    c_enum_id,
    results_enumerator_folder,
    results_features_folder,
    results_shap_folder,
)
import shap

    
    
def plot_shap_interpretation(shap_values, features):
    fig1 = plt.figure()
    shap.summary_plot(shap_values, features)
    fig1.figure.savefig(results_shap_folder / "shap1.pdf")
    
    fig2 = plt.figure()
    shap.summary_plot(shap_values, features, plot_type="bar")
    fig2.figure.savefig(results_shap_folder / "shap2.pdf")
    
    
def plot_suspicious_enumerators(enumerators):
    # Plot enumerators by suspicion
    fig = plt.figure()
    sns.barplot(
        data=enumerators, x=c_enum_id, y="percent_sus", hue="region", dodge=False
    ).set(xticklabels=[], xlabel="All Enumerators")
    fig.savefig(results_enumerator_folder /"enumerators_by_suspicion.pdf")
    
    
def plot_typical_vs_anomalies(surveys_with_IF_scores, feature_names):
    for names in feature_names:
        fig = plt.figure()
        sns.violinplot(x="survey_type", y=names, data=surveys_with_IF_scores)
        fig.savefig(results_features_folder / (names + ".pdf"))

