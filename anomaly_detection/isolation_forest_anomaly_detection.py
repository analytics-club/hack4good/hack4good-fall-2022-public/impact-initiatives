from anomaly_detection.isolation_forest_and_helpers import (
    combine_survey_with_enumerator_features,
    isolation_forest,
    shap_intepretation,
)

from pandas import DataFrame

from constants import c_enum_id
from storage_handler import write_final_results_to_excel, prepare_results_folder
from plotter import  (
    plot_shap_interpretation, 
    plot_suspicious_enumerators, 
    plot_typical_vs_anomalies,
)


def perform_and_save_anomaly_detection(
    enumerators: DataFrame, surveys: DataFrame, desired_survey_features, plot=True
):
    # Combine relevant enumerator and Survey features
    features, enumerator_list = combine_survey_with_enumerator_features(
        surveys, enumerators, desired_survey_features
    )

    # Run Isolation Forest
    surveys_with_IF_scores, IF = isolation_forest(features, enumerator_list)

    # Interpet with SHAP Values
    surveys_IF_shap, shap_values = shap_intepretation(
        features, surveys_with_IF_scores, IF
    )

    # Select features that are
    IF_outliers = surveys_with_IF_scores[
        surveys_with_IF_scores.survey_type == "anomaly"
    ]
    IF_typical = surveys_with_IF_scores[surveys_with_IF_scores.survey_type != "anomaly"]
    outliers_shap = surveys_IF_shap[surveys_with_IF_scores.survey_type == "anomaly"]

    # See which enumerators are most suspicious
    enumerators = enumerators.join(IF_outliers.global_enum_id.value_counts()).fillna(0)
    enumerators = enumerators.rename(columns=({c_enum_id: "num_suspicious"}))

    enumerators["percent_sus"] = (
        enumerators["num_suspicious"] / enumerators["num_surveys"]
    )
    enumerators["region"] = enumerators.index.str.split("_").str[0]
    enumerators[c_enum_id] = enumerators.index

    typical_summary = IF_typical.describe()

    anomalies_summary = IF_outliers.describe()

    

    # Output final results to an excel file and save plots to pdf
    prepare_results_folder()
    
    if(plot):
        plot_shap_interpretation(shap_values, features)
        plot_suspicious_enumerators(enumerators)
        plot_typical_vs_anomalies(surveys_with_IF_scores, desired_survey_features)
    
    
    
    write_final_results_to_excel(
        IF_outliers, outliers_shap, enumerators, typical_summary, anomalies_summary
    )
