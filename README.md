# IMPACT Initiatives

Dependencies: scipy numpy pandas matplotlib pyarrow openpyxl scikit-learn shap

To run: (*The steps 2-3 can be executed by running `main.py`*)
1. Place `data_to_share` folder in the parent directory of this repository.
2. Run `feature_engineering.make_featurized_dataframes()` to unzip, collect, and featurize data.
3. Run `anomaly_detection.isolation_forest_anomaly_detection.perform_and_save_anomaly_detection()` to identify anomalies and save the results to an excel file.

> `.make_featurized_dataframes()` stores the featurized data it has generated to disk. Thus, subsequent runs can instead call `storage_handler.load_processed()` to reuse the featurized data.

**When running the analysis on a new set of surveys make sure to update the variables with prefix `src_` in `constants.py` to match the proper paths, filenames and sheet names of this new set of surveys.**

**The features to be used by the Anomaly Detection Algorithm can easily be edited via altering the list of variables in `desired_features_anomaly_detection` within the `main.py` script.**

A run of `.make_featurized_dataframes()` looks as follows:
1. _If destination files don't already exist_: Load survey responses and all corresponding audit files, and store these to `./data/` in the more efficient `.parquet` file format. Then combine all the audit DataFrames of individual survey responses into a single large one, and store this to `./data/`.
1. Load the survey responses DataFrame (`surveys`) and the combined audits DataFrame (`audits`) created in step 1, and initialize the DataFrames `enumerators`, `nodes`, and `deindexed_nodes` without any features (only containing columns needed to join them with each other).
1. Compute and attach features for `enumerators`, `surveys`, `audits`, `nodes`, and `deindexed_nodes`, and store these to `./data/`.

> Note that step 1 here is only performed the first time `.make_featurized_dataframes()` is run, since the destination files will already exist on subsequent runs. This saves a lot of time when re-running `.make_featurized_dataframes()` after only tweaking the feature generation code, but does mean that the whole `./data/` folder should be deleted after a change to the source data or data collection process has occurred, since that change will otherwise be ignored.

## Result Files
The final results output to `results/anomaly_results_summary.xlsx`. There are five sheets within this file:
1. **Anomalies**: contains the uuid and feature values for every survey flagged as an anomaly. Additionally, the the Isolation score is listed as well as the three most important features that resulted in this given survey being classified as an anomaly.
2. **SHAP Values**: Contains the SHAP values for every feature for every survey
3. **Enumerators**: Contains the IDs of all enumerators as well as the number and percent of suspicious surveys they completed.
4. **Typical Survey Features**: Contains the summary statistics for all features, among those surveys classified as typical
5. **Anomaly Survey Features**: Contains the summary statistics for all features, among those surveys classified as anomalies

The code as plots the distribtions features for typical surveys compared to anomaly surveys, the feature importance by SHAP value, and the percent of suspicious surveys filled out by each enumerator grouped by base city. This is currently carried out within `plotter.py` and saves the figures within the `results` folder


## Feature engineering
The calculation of features is handled within the `feature_engineering` folder.

The responsibilities are as follows:
1. `__init__.py` contains all public functions (`.make_featurized_dataframes` and `.attach_features`), and acts as a main entry point and coordinator, calling the functions to have the DataFrames initialized and features added.
2. `bootstrapper.py` is responsible for initializing all the DataFrames, which means that it is also responsible for calling `storage_handler` to collect the raw surveys and audits data.
3. The files with postfix `_handler` (e.g. `surveys_handler.py` and `audits_handler.py`) are each responsible for engineering features for the DataFrame in their name, so e.g. `surveys_handler.py` generates all features for the `surveys` DataFrame, while `audits_handler.py` generate those for the `audits` DataFrame.

And the data is partitioned as follows:
- `enumerators`: One row = one enumerator (indexed by `global_enum_id`).
- `surveys`: One row = one survey response (index by `uuid`).
- `audits`: One row = one page visit in app (same as in the original audit files).
- `nodes`: One row = one unique node value in `audits` (indexed by `node`).
- `deindexed_nodes`: One row = one unique deindexed node value in `audits`, i.e. a node value with all index references (e.g. `[1]`) removed.

Documentation detailing every feature and where to find it can be found in `Final_Feature_Documentation.pdf`.

Only a few of the features have been investigated throughly to ensure they aren't dictated by outliers or otherwise behaving unexpectedly, so if something seems off about any of the feature values we strongly recommend investigating them and taking appropriate action. In general, it is possible and even recommended to add, remove, or otherwise edit features as needs arise. This is not a static final solution, but rather a framework on which to expand.

## Anomaly detection

### Isolation forests
The isolation forest algorithm isolates each observation by randomly selecting one of the features, and randomly selecting a value between the min and the max on which to split. The number of splits required to isolate an observation, averaged over a forest of such random trees, measures the normality of the observation: anomalies should have noticeable shorter paths (since they are isolated more easily), and therefore consistent short path lengths imply a high probability of being an anomaly.

Read more about the isolation forest implementation here: https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.IsolationForest.html 

### Interpretation
SHAP (SHapley Additive exPlanations) was used to provide interpretations for the Isolation Forests. Documentation on SHAP can be found here https://scikit-learn.org/stable/modules/permutation_importance.html.

It is also possible to use other methods, such as Permutation Importance to interpret the predictions of an isolation forest. Documentation about this method can be found here https://scikit-learn.org/stable/modules/permutation_importance.html. However, because an Isolation Forest is not a traditional Random Forest, one must first fit a Random Forest Model on the IF-scores outputted by the Isolation Forest. Then Permutation Importance can be used on this fitten Random Forest to acheive reliable interpretations. 

### Extensions
There are several other methods that can be used to detect anomalies:
1. Overview of commonly used techniques: https://en.wikipedia.org/wiki/Anomaly_detection
2. Heuristic methods such as setting thresholds on singular features based on prior knowledge of what behavior is suspicious
