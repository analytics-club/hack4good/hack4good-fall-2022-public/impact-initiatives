import numpy as np
from pandas import DataFrame

from constants import c_deindexed_median_seconds, c_seconds, c_relative_pace, c_deindexed_node, c_node, c_dur


def attach_durations(audits: DataFrame):
    """:return audits with `seconds` (seconds between start and end)."""
    audits[c_seconds] = (audits.end - audits.start) / 1000
    audits[c_dur] = np.sqrt(np.log(np.maximum(1, audits[c_seconds])))
    return audits


def attach_relative_pace(audits: DataFrame, nodes: DataFrame, deindexed_nodes: DataFrame):
    """:return audits with `relative_pace` (median response time divided by this response time)"""
    joined = audits.join(nodes, on=c_node).join(
        deindexed_nodes, on=c_deindexed_node, rsuffix="_"
    )
    audits[c_relative_pace] = joined[c_deindexed_median_seconds] / joined[c_seconds]
    return audits
