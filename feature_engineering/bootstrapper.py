import re

from pandas import DataFrame

from constants import c_enum_id, c_node, c_deindexed_node
from storage_handler import cached_collect_merged_raw_data


def initialize_dataframes():
    """:return enumerators, surveys, audits, nodes, deindexed_nodes,
    with only the columns required to join them with each other,
    collecting the required source data if necessary."""
    surveys, audits, questionnaire = cached_collect_merged_raw_data()
    surveys = surveys[["start", c_enum_id]]
    enumerators = DataFrame(index=surveys[c_enum_id].unique())
    nodes = DataFrame(index=audits[c_node].unique())
    nodes[c_deindexed_node] = nodes.index.to_series().apply(_extract_deindexed)
    deindexed_nodes = DataFrame(index=nodes[c_deindexed_node].unique())
    return enumerators, surveys, audits, nodes, deindexed_nodes, questionnaire


def _extract_deindexed(x):
    """:return `x` with all indeces removed."""
    return re.sub(r"\[\d+]", "", x) if isinstance(x, str) else x
